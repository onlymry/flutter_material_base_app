import 'package:flutter/material.dart';

class Mine extends StatefulWidget {
  Mine({Key key}) : super(key: key);
  MineBase createState() => MineBase();
}

class MineBase extends State<Mine> {
  Widget build(context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Mine'),
        actions: [
          IconButton(
            icon: Icon(Icons.person),
            onPressed: () {
              Navigator.of(context).pushNamed('/userinfo');
            },
          )
        ],
      ),
      body: Container(),
    );
  }
}
