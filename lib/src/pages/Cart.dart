import 'package:flutter/material.dart';

class Cart extends StatefulWidget {
  Cart({Key key}) : super(key: key);
  CartBase createState() => CartBase();
}

class CartBase extends State<Cart> {
  //选择全部商品
  void _selectGoods() {}
  Widget build(context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cart'),
        actions: <Widget>[
          IconButton(
            icon: Icon(IconData(0xe7e9, fontFamily: 'tbicon')),
            onPressed: this._selectGoods,
          )
        ],
      ),
      body: Container(),
    );
  }
}
