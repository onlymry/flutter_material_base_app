import 'package:flutter/material.dart';
import '../../components/alertDialog.dart';
import '../../components/locationBuilder.dart';
import '../../components/snackBar.dart';

class DebugMode extends StatefulWidget {
  DebugMode({Key key}) : super(key: key);
  DebugModeBase createState() => DebugModeBase();
}

class DebugModeBase extends State<DebugMode> {
  Widget build(context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('APP DEBUG'),
      ),
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: Wrap(
          spacing: 8.0,
          runSpacing: 8.0,
          children: [
            FlatButton(
              child: Text('webview'),
              textColor: Colors.white,
              color: Colors.lightBlue,
              onPressed: () {
                Navigator.of(context).pushNamed('/webview',
                    arguments: {"url": "https://m.fengniao.com/bbs/"});
              },
            ),
            FlatButton(
              child: Text('image preview'),
              textColor: Colors.white,
              color: Colors.lightBlue,
              onPressed: () {
                Navigator.of(context).pushNamed('/imagepreview');
              },
            ),
            FlatButton(
              child: Text('anthor'),
              textColor: Colors.white,
              color: Colors.lightBlue,
              onPressed: () {},
            ),
            LocationsWidget(),
            FlatButton(
              child: Text('ErrorPages'),
              textColor: Colors.white,
              color: Colors.lightBlue,
              onPressed: () {
                Navigator.of(context).pushNamed('/error');
              },
            ),
            FlatButton(
                onPressed: () {
                  SnakerBarBuilder().builder(context, 'snakerBar', () {
                    print('snaker bar 关闭以后执行');
                  });
                },
                textColor: Colors.white,
                color: Colors.lightBlue,
                child: const Text('snakebar')),
            FlatButton(
                onPressed: () {
                  AlertDialogBuilder()
                      .show(context, '提示', [Text('alertdialog')]);
                },
                textColor: Colors.white,
                color: Colors.lightBlue,
                child: const Text('alertdialog'))
          ],
        ),
      ),
    );
  }
}
