import 'package:flutter/material.dart';

class ImagePreview extends StatefulWidget {
  ImagePreview({Key key}) : super(key: key);
  ImagePreviewBase createState() => ImagePreviewBase();
}

class ImagePreviewBase extends State<ImagePreview> {
  /*
   * tab页面控制器
   */
  PageController _controller = new PageController();
  final List _imageList = [
    "https://bbs.qn.img-space.com/202008/27/ae5266e659855218834fded84e1ea010.jpg",
    "https://bbs.qn.img-space.com/202008/27/75c8b4027bafe2c5ea6c4d04687c327c.jpg",
    "https://bbs.qn.img-space.com/202008/27/b9202411eec581f3487bf660815f4d53.jpg",
    "https://bbs.qn.img-space.com/202006/15/d2cf75074918246b8a1624b81551873e.jpg",
    "https://bbs.qn.img-space.com/202006/15/116e0421b80572f85b5590c624fce689.jpg",
    "https://bbs.qn.img-space.com/202005/13/5e6df73eaa82738cf6d873359eda5e09.jpg",
    "https://bbs.qn.img-space.com/202004/20/0d6889667878c41b44681161aea1b845.jpg",
    "https://bbs.qn.img-space.com/202008/27/367cceda4c565bdeea6710033f555cb9.jpg"
  ];
  List<Widget> _builderImageBox(context) {
    return List<Widget>.from(this._imageList.asMap().keys.map((index) {
      return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Image.network(
          this._imageList[index],
          fit: BoxFit.fitWidth,
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
        ),
      );
    })).toList();
  }

  int _current = 1;
  int _all = 0;
  TextStyle _style = new TextStyle(
      color: Color(0xffffffff), fontWeight: FontWeight.w600, fontSize: 32);

  void initState() {
    if (mounted) {
      setState(() {
        this._all = this._imageList.length;
      });
    }
    super.initState();
  }

  void _changeEvent(index) {
    if (mounted) {
      setState(() {
        this._current = index + 1;
      });
    }
  }

  Widget build(context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
          elevation: 0.0,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Color(0xffffffff),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          title: Text(
            'imagePreview',
            style: _style,
          ),
          backgroundColor: Colors.transparent),
      body: Container(
        color: Color(0xff000000),
        child: Stack(
          children: [
            PageView(
              controller: _controller,
              children: _builderImageBox(context),
              onPageChanged: _changeEvent,
            ),
            Positioned(
              bottom: 30,
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      '$_current',
                      style: _style,
                    ),
                    Text(
                      '/',
                      style: _style,
                    ),
                    Text(
                      '$_all',
                      style: _style,
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

//
