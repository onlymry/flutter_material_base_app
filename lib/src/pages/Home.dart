import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';

import '../../components/swiperBuilder.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);
  HomeBase createState() => HomeBase();
}

class HomeBase extends State<Home> {
  /**
   * swiper List
   */
  List<Map> _swiperList = [
    {
      'url':
          "https://bbs.qn.img-space.com/202008/27/ae5266e659855218834fded84e1ea010.jpg"
    },
    {
      'url':
          "https://bbs.qn.img-space.com/202008/27/367cceda4c565bdeea6710033f555cb9.jpg"
    },
    {
      'url':
          "https://bbs.qn.img-space.com/202004/20/0d6889667878c41b44681161aea1b845.jpg"
    }
  ];
  /*
  * 点击搜索按钮导航到搜索页面
  */
  void _searchEvent(context) {
    print('search');
    Navigator.of(context).pushNamed('/search');
  }

  /*
  * 扫描二维码
  */
  void _scanEvent() async {
    var result = await BarcodeScanner.scan();

    print(result.type); // The result type (barcode, cancelled, failed)
    print(result.rawContent); // The barcode content
    print(result.format); // The barcode format (as enum)
    print(result.formatNote);
  }

  Widget build(context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        actions: <Widget>[
          IconButton(
            icon: Icon(IconData(0xe689, fontFamily: 'tbicon')),
            onPressed: () {
              print('scan');
              this._scanEvent();
            },
          ),
          IconButton(
            icon: Icon(IconData(0xe65c, fontFamily: 'tbicon')),
            onPressed: () {
              this._searchEvent(context);
            },
          ),
        ],
      ),
      // drawer: Drawer(), //抽屉
      body: Container(
        child: ListView(
          children: [builderSwiper(context, this._swiperList)],
        ),
      ),
    );
  }
}

Widget builderSwiper(BuildContext context, list) {
  return Container(
    width: MediaQuery.of(context).size.width,
    height: 200,
    child: SwiperWidget(
      imgurl: list,
    ),
  );
}
