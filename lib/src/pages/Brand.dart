import 'package:flutter/material.dart';

class Brand extends StatefulWidget {
  Brand({Key key}) : super(key: key);
  BrandBase createState() => BrandBase();
}

class BrandBase extends State<Brand> {
  List<String> _tabs = ['One', 'Two', 'Three'];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: _tabs.length,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('TabBar Sample'),
          bottom: TabBar(
            tabs: _tabs.map((String name) => Tab(text: name)).toList(),
          ),
        ),
        body: TabBarView(
          children: _tabs.map((String name) {
            return SafeArea(
              top: false,
              bottom: false,
              child: Builder(
                builder: (BuildContext context) {
                  return Center(
                    child: Text('Page $name'),
                  );
                },
              ),
            );
          }).toList(),
        ),
      ),
    );
  }
}
