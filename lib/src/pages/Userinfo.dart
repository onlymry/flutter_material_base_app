import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class UserInfomation extends StatefulWidget {
  UserInfomation({Key key}) : super(key: key);
  UserInfomationBase createState() => UserInfomationBase();
}

class UserInfomationBase extends State<UserInfomation> {
  /*
   * file 类型的图片 
   */
  File _path;
  // 底部弹出图片sheets;
  // final _image = new ImagePickerBottomSheets();

  // picker
  final _picker = ImagePicker();
  //获取照片
  Future _getImage(source) async {
    final pickedFile = await _picker.getImage(source: source);
    if (pickedFile.path == null) {
      return;
    } else {
      return pickedFile;
    }
  }

  void _showPickerSheets(context) {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return _buildSelectorImagePicker(context, (res) {
            if (mounted) {
              setState(() {
                this._path = File(res.path);
              });
              Navigator.of(context).pop();
            }
          });
        });
  }

  Widget _buildSelectorImagePicker(context, cb) {
    return Container(
      height: 180,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(8.0), topRight: Radius.circular(8.0)),
        color: Color(0xfffffffff),
      ),
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          InkWell(
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 60,
              child: Center(
                child: Text('从相册选择'),
              ),
            ),
            onTap: () {
              // cb();
              this._getImage(ImageSource.gallery).then((value) {
                cb(value);
              });
            },
          ),
          InkWell(
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 60,
              child: Center(
                child: Text('拍照'),
              ),
            ),
            onTap: () {
              this._getImage(ImageSource.camera).then((value) {
                cb(value);
              });
            },
          ),
          InkWell(
            child: Container(
                width: MediaQuery.of(context).size.width,
                height: 60,
                child: Center(
                  child: Text('关闭'),
                )),
            onTap: () {
              Navigator.pop(context);
            },
          )
        ],
      ),
    );
  }

  Widget build(context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('用户信息'),
        actions: [
          IconButton(
            icon: Icon(
              IconData(0xe724, fontFamily: 'tbicon'),
            ),
            onPressed: () {
              // 唤醒bottomsheets
              // 底部弹框选择获取图片的方式
              this._showPickerSheets(context);
            },
          ),
        ],
      ),
      body: Container(
        child: Column(
          children: [
            Container(
              child: this._path == null ? Container() : Image.file(this._path),
            )
          ],
        ),
      ),
    );
  }
}
