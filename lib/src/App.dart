import 'package:flutter/material.dart';
import '../components/bottomNavigatorBuilder.dart';

import './pages/Home.dart';
import './pages/Category.dart';
import './pages/Brand.dart';
import './pages/Cart.dart';
import './pages/Mine.dart';
import './pages/Debug.dart';

class App extends StatefulWidget {
  App({Key key}) : super(key: key);
  AppBase createState() => AppBase();
}

class AppBase extends State<App> {
  /*
   * tab页面控制器
   */
  PageController _controller = new PageController();
  /*
   * 当前页面index
   */
  int _currentPageIndex = 0;
  /*
   * 底部菜单列表 
  */
  List _bottomBarList = [
    {"icon": Icon(IconData(0xe6b8, fontFamily: 'Tbicon')), 'label': '首页'},
    {"icon": Icon(IconData(0xe682, fontFamily: 'Tbicon')), 'label': '分类'},
    {"icon": Icon(IconData(0xe809, fontFamily: 'Tbicon')), 'label': '品牌'},
    {"icon": Icon(IconData(0xe6cd, fontFamily: 'Tbicon')), 'label': '购物车'},
    {"icon": Icon(IconData(0xe736, fontFamily: 'Tbicon')), 'label': '我的'},
    {
      "icon": Icon(
        Icons.bug_report,
        color: Color(0xfffff0000),
      ),
      'label': 'DEBUG'
    },
  ];
  /*
  * tab页面
  */
  List<Widget> _tabPages = [
    Home(),
    Category(),

    // Home(),
    Brand(),
    Cart(),
    Mine(),
    DebugMode()
  ];
  /*
   *  initState 
   */
  void initState() {
    super.initState();
  }

  /*
   * 底部导航点击事件
   */
  void _bottomClickEvent(index) {
    if (mounted) {
      setState(() {
        this._currentPageIndex = index;
        print(index);
        this._controller.animateToPage(index,
            duration: Duration(milliseconds: 200), curve: Curves.bounceInOut);
      });
    }
  }

  /*
   * 滑动切换页面
   */
  void _pageChange(index) {
    if (mounted) {
      setState(() {
        this._currentPageIndex = index;
      });
    }
  }

  Widget build(context) {
    return Scaffold(
      body: PageView(
        controller: _controller,
        children: _tabPages,
        onPageChanged: _pageChange,
      ),
      bottomNavigationBar: BottomTabBar(
        listItem: this._bottomBarList,
        tabIndex: this._currentPageIndex,
        onTap: this._bottomClickEvent,
      ),
    );
  }
}
