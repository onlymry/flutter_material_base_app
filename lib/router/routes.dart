import 'package:flutter/material.dart';

import '../src/App.dart';
import '../src/pages/Search.dart';
import '../src/pages/Collections.dart';
import '../src/pages/Userinfo.dart';
import '../src/pages/webviewBuilder.dart';
import '../src/pages/imagePreview.dart';
import '../src/pages/Error.dart';

Map<String, WidgetBuilder> routes = {
  '/': (context) => App(),
  '/search': (context) => Search(),
  '/collections': (context) => Collections(),
  '/userinfo': (context) => UserInfomation(),
  '/webview': (context) =>
      WebViewCommponent(url: ModalRoute.of(context).settings.arguments),
  '/imagepreview': (context) => ImagePreview(),
  '/error': (context) => ErrorPage()
};
