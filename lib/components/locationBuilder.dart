import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:location/location.dart';

class LocationsWidget extends StatefulWidget {
  LocationsWidget({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LocationsWidgetBuilder();
  }
}

class LocationsWidgetBuilder extends State<LocationsWidget> {
  Location location = new Location();

  bool _serviceEnabled;
  PermissionStatus _permissionGranted;
  LocationData _locationData;
  String _error = '';

  Future<void> _checkService() async {
    final bool serviceEnabledResult = await location.serviceEnabled();
    setState(() {
      _serviceEnabled = serviceEnabledResult;
    });
    print(this._serviceEnabled);
  }

  Future<void> _requestService() async {
    if (_serviceEnabled == null || !_serviceEnabled) {
      final bool serviceRequestedResult = await location.requestService();
      setState(() {
        _serviceEnabled = serviceRequestedResult;
      });
      if (!serviceRequestedResult) {
        return;
      }
    }

    print(this._serviceEnabled);
  }

  Future<void> _checkPermissions() async {
    final PermissionStatus permissionGrantedResult =
        await location.hasPermission();
    setState(() {
      _permissionGranted = permissionGrantedResult;
    });
    print(_permissionGranted);
  }

  Future<void> _requestPermission() async {
    if (_permissionGranted != PermissionStatus.granted) {
      final PermissionStatus permissionRequestedResult =
          await location.requestPermission();
      setState(() {
        _permissionGranted = permissionRequestedResult;
      });
      print(_permissionGranted);
    }
  }

  Future<void> _getLocation() async {
    setState(() {
      _error = '';
    });
    try {
      final LocationData _locationResult = await location.getLocation();
      setState(() {
        _locationData = _locationResult;
      });

      print(this._locationData);
    } on PlatformException catch (err) {
      setState(() {
        _error = err.code;
      });
    }
  }

  Widget build(context) {
    return Container(
      child: Column(
        children: [
          Text('获取定位信息'),
          OutlineButton(
            onPressed: () {
              this._checkService();
            },
            child: Text('checkService-检查服务'),
          ),
          OutlineButton(
            onPressed: () {
              this._requestService();
            },
            child: Text('_requestService'),
          ),
          OutlineButton(
            onPressed: () {
              this._requestPermission();
            },
            child: Text('_requestPermission-检查权限'),
          ),
          OutlineButton(
            onPressed: () {
              this._checkPermissions();
            },
            child: Text('_checkPermissions'),
          ),
          OutlineButton(
            onPressed: () {
              this._getLocation();
            },
            child: Text('getLocations-获取地址'),
          ),
        ],
      ),
    );
  }
}
