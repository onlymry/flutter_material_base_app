import 'package:flutter/material.dart';
import 'scrollBuilder.dart';

class TransportAppBarPageWidget extends StatefulWidget {
  TransportAppBarPageWidget({
    Key key,
    @required this.onScroll, //页面滚动事件
    @required this.onToBottom, //页面到底事件
    @required this.onToTop, //页面触顶事件
    @required this.child, //子级widget
    this.isLoading = false, //加载中
    this.isiOS = false, //使用ios样式loading
    this.isRefresh = true, //刷新
    this.actions, //appbar Actions
    this.leading, //appbar Leading
    this.bottom, //AppBar Bottom
    this.shape, //AppBar Shape
    this.centerTitle = false, //标题居中
    this.elevation = 0.0, //AppBar elevation
    this.title, //appBar标题
    this.drawer, //左边抽屉
    this.floatingActionButton, //浮动按钮
    this.endDrawer, //右边抽屉
    this.bottomNavigationBar, //底部导航
  }) : super(key: key);

  final Widget child;
  final bool isLoading;
  final bool isiOS;
  final bool isRefresh;
  final onScroll;
  final onToBottom;
  final onToTop;
  final Widget title;
  final Widget leading;
  final List<Widget> actions;
  final PreferredSizeWidget bottom;
  final double elevation;
  final ShapeBorder shape;
  final bool centerTitle;
  final Widget drawer;
  final Widget floatingActionButton;
  final Widget endDrawer;
  final Widget bottomNavigationBar;
  @override
  State<StatefulWidget> createState() => TransportAppBarPageWidgetBuilder();
}

class TransportAppBarPageWidgetBuilder
    extends State<TransportAppBarPageWidget> {
  double _presetOffset = 200.0;
  // opcity偏移
  int _opacity = 0;
  // opcity
  bool showToTopBtn = false;
  //是否显示“返回到顶部”按钮

  ScrollController controller = new ScrollController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // this.controller.dispose();
    super.dispose();
  }

  // 滚动监听
  void scrollListener(controller) {
    controller.addListener(() {
      // 当前返回顶部的btn是否显示
      if (controller.offset < 1000 && showToTopBtn) {
        if (mounted) {
          setState(() {
            showToTopBtn = false;
          });
        }
      } else if (controller.offset >= 1000 && showToTopBtn == false) {
        if (mounted) {
          setState(() {
            showToTopBtn = true;
          });
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: widget.elevation,
        centerTitle: widget.centerTitle,
        actions: widget.actions,
        bottom: widget.bottom,
        shape: widget.shape,
        leading: widget.leading,
        backgroundColor: Theme.of(context).primaryColor.withAlpha(_opacity),
        title: widget.title,
      ),
      drawer: widget.drawer,
      endDrawer: widget.endDrawer,
      bottomNavigationBar: widget.bottomNavigationBar,
      floatingActionButton: widget.floatingActionButton ??
          (this.showToTopBtn == true
              ? FloatingActionButton(
                  child: Icon(Icons.expand_less),
                  onPressed: () {
                    this.controller.animateTo(.0,
                        duration: Duration(milliseconds: 500),
                        curve: Curves.ease);
                  },
                )
              : null),
      body: SingleChildScrollViewBuilder(
        onScrollEvent: (controll) {
          if (mounted) {
            setState(() {
              // 改变opcity
              if (controll.offset >= 0) {
                // 如果当前App platfrom是iOS的就会出现offset<0的情况，所以这里判断这个问题是否存在。
                _opacity = controll.offset > _presetOffset
                    ? 255
                    : controll.offset * 255 ~/ _presetOffset;
              } else {
                _opacity = 0;
              }
              this.controller = controll;
            });
          }
          widget.onScroll(controll);
          this.scrollListener(controll);
        },
        onToBottomEvent: widget.onToBottom,
        onToTopEvent: widget.onToTop,
        child: widget.child,
        isLoading: widget.isLoading,
        isiOS: widget.isiOS,
        isrefresh: widget.isRefresh,
      ),
    );
  }
}
