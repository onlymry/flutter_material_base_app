import 'package:flutter/material.dart';

final Duration _duration = Duration(milliseconds: 1000);

class SnakerBarBuilder {
  void builder(context, message, cancel, {duration}) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        duration: duration ?? _duration,
        content: Text(message),
        action: SnackBarAction(
          label: '取消',
          onPressed: cancel,
        ),
      ),
    );
  }
}
