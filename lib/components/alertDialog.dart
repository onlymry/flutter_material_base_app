import 'package:flutter/material.dart';

class AlertDialogBuilder {
  Future<void> show(context, title, children,
      {cancel, ok, cancelEvent, okEvent}) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: ListBody(
              children: children,
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(cancel ?? 'cancel'),
              onPressed: () {
                if (cancelEvent != null) cancelEvent();
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text(ok ?? 'ok'),
              onPressed: () {
                if (okEvent != null) okEvent();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
