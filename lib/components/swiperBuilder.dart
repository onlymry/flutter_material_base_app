import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class SwiperWidget extends StatefulWidget {
  SwiperWidget(
      {Key key, this.hasControl = false, this.imagePreview = true, this.imgurl})
      : super(key: key);
  final bool hasControl;
  final bool imagePreview;
  final List<Map> imgurl;

  @override
  State<StatefulWidget> createState() {
    return SwiperWidgetBuilder();
  }
}

class SwiperWidgetBuilder extends State<SwiperWidget> {
  Widget build(BuildContext context) {
    return Swiper(
      itemBuilder: (BuildContext context, int index) {
        return buidlerSwiperItem(widget.imgurl, context, index);
      },
      itemCount: widget.imgurl.length,
      pagination: new SwiperPagination(),
      control: widget.hasControl ? new SwiperControl() : null,
      onTap: (index) {
        if (widget.imagePreview) {
          List<Map> list = widget.imgurl;
          Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => ImagePreview(url: list[index]['url']),
          ));
        } else {
          print(index);
        }
      },
    );
  }
}

Widget buidlerSwiperItem(List<Map> imgurl, BuildContext context, int index) {
  return Hero(
      tag: 'swiper',
      child: Image.network(
        imgurl[index]['url'],
        fit: BoxFit.cover,
      ));
}

/**
 * 图片预览
 */
class ImagePreview extends StatefulWidget {
  ImagePreview({Key key, @required this.url}) : super(key: key);

  final String url;

  @override
  _ImagePreviewState createState() => _ImagePreviewState();
}

class _ImagePreviewState extends State<ImagePreview> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // 设置透明导航栏
        extendBodyBehindAppBar: true,
        appBar: AppBar(
            elevation: 0.0,
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Color(0xffffffff),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
              color: Color.fromRGBO(255, 255, 255, 1),
            ),
            backgroundColor: Colors.transparent),
        body: Container(
          // 图片容器
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: Color(0xff000000),
          child: Hero(
            tag: 'swiper',
            child: Image.network(
              widget.url,
              fit: BoxFit.fitWidth,
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
            ),
          ),
        ));
  }
}
