# flutter_material_base_app

A Base Flutter project.

## Getting Started

```bash 

git clone https://gitee.com/onlymry/flutter_material_base_app.git

cd flutter_material_base_app

flutter pub get

flutter run

```
## Plugins

|名称|版本|功能|
--|:--:|--:
|barcode_scan| ^3.0.1   |扫码|
|image_picker|^0.6.7+4|图片选择和拍照|
|webview_flutter|^0.3.22+1|webview|
|flutter_swiper|^1.1.6|轮播图|
|location|^3.0.2|定位|
